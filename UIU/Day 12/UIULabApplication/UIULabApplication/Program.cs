﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIULabApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            GarmentsDbEntities db = new GarmentsDbEntities();

            //List<Attendance> attendances = db.Attendances.ToList();
            //foreach (Attendance attendance in attendances)
            //{
            //    Console.WriteLine(attendance.Worker.Name + " Date:" + attendance.AttendanceDay + " In and out count:" + attendance.InAndOuts.Count + " Total hours worked:" + attendance.TotalHour);
            //    foreach (InAndOut inAndOut in attendance.InAndOuts)
            //    {
            //        var type = inAndOut.IsInTime ? "In: " : "Out: ";
            //        Console.WriteLine(type + " " + inAndOut.PunchTime.TimeOfDay);
            //    }
            //}

            Console.WriteLine("Please enter the name of the worker:");
            string name = Console.ReadLine();
            name = name.ToUpper();
            int id = db.Workers.First(x => x.Name.ToUpper() == name).Id;

            Console.WriteLine("Please enter the punch time detail below.");
            Console.WriteLine("Punch type is In? y/n");
            var punchTypeIsIn = Console.ReadLine().ToUpper() == "Y";
            var today = DateTime.Today;
            var attendance =
                db.Attendances.FirstOrDefault(x => x.WorkerId == id && x.AttendanceDay == today);
            if (attendance == null)
            {
                attendance = new Attendance()
                {
                    AttendanceDay = DateTime.Today,
                    TotalHour = 0,
                    WorkerId = id
                };
                db.Attendances.Add(attendance);
            }

            db.InAndOuts.Add(new InAndOut()
            {
                IsInTime = punchTypeIsIn,
                PunchTime = DateTime.Now,
                AttendanceId = attendance.Id
            });
            db.SaveChanges();

            if (!punchTypeIsIn)
            {
                int sum = 0;
                var inAndOuts = db.InAndOuts.Where(x => x.AttendanceId == attendance.Id).OrderByDescending(o => o.PunchTime).ToList();
                for (int i = 0; i < inAndOuts.Count; i = i + 2)
                {
                    TimeSpan timeSpan = inAndOuts[i].PunchTime - inAndOuts[i + 1].PunchTime;
                    sum += timeSpan.Seconds;
                }
                db.Attendances.First(a => a.Id == attendance.Id).TotalHour = sum;               
                db.SaveChanges();
            }

            Console.WriteLine("saved");
            Console.Read();
        }
    }
}
