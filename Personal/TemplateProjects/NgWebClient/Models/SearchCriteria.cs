﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NgWebClient.Models
{
    public class SearchCriteria
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}