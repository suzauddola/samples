﻿(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/home/home.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.
            
        },

        init: function(element,options) {

            this.oniteminvoked = WinJS.UI.eventHandler(this.oniteminvokedimplementation.bind(this));
        },

        oniteminvokedimplementation: function (selectedItem) {
            var value = Data.list.getAt(selectedItem.detail.itemIndex);
            WinJS.Navigation.navigate('/pages/products/products.html',value);
        }
    });
})();
