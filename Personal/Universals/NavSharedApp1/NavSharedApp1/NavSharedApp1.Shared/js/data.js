﻿(function() {
    "use strict";

    WinJS.Namespace.define("Data", {
        list: getGroupList()
    });


    function getGroupList() {
        var data = new WinJS.Binding.List([]);
        var bbq = {
            code: '1',
            name: 'Bar-b-q-tonite',            
            image1: "/images/bbq.jpg",
            starters: new WinJS.Binding.List([
                { code: 'bbq1', name: 'Samosa', price: 10, image: '/images/samosa.jpg' },
                { code: 'bbq2', name: 'Singara', price: 12, image: '/images/singara.jpg' }
            ]),
            rice: new WinJS.Binding.List([
                { code: 'bbq3', name: 'Biriyani', price: 10, image: '/images/biriyani.jpg' },
                { code: 'bbq4', name: 'Beef Khichuri', price: 12, image: '/images/khicuri.jpg' }
            ])
        };

        var baburchi = {
            code: '1',
            name: 'Bar-b-q-tonite',
            image: "/images/baburchi.jpg",
            starters: new WinJS.Binding.List([
                { code: 'b1', name: 'Samosa', price: 10, image: '/images/samosa2.jpg' },
                { code: 'b2', name: 'Singara', price: 12, image: '/images/singara2.jpg' }
            ]),
            rice: new WinJS.Binding.List([
                { code: 'b3', name: 'Biriyani', price: 10, image: '/images/biriyani2.jpg' },
                { code: 'b4', name: 'Beef Khichuri', price: 12, image: '/images/khicuri2.jpg' }
            ])

        };


        data.push(bbq);
        data.push(baburchi);
        return data;
    }

})();